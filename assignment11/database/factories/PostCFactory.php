<?php

namespace Database\Factories;

use App\Models\Post-c;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostCFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post-c::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
