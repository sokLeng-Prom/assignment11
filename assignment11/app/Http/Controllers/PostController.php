<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\Category;

class PostController extends Controller
{ public function index() {
    $posts = Post::all();

    return view('post', [
        'posts' => $posts
    ]);
}

public function create() {
    $categories = Category::all();

    return view('post-create', [
        'categories' => $categories
    ]);
}

public function edit(Request $request, $id) {
    $post = Post::find($id);

    $post->title = $request->get('title');
    $post->body = $request->get('body');
    $post->category_id = $request->get('category_id');
    $post->save();

    return redirect(route('posts'));
}

public function editPage($id) {
    $post = Post::find($id);
    $categories = Category::all();

    return view('post-edit', [
        'post' => $post,
        'categories' => $categories
    ]);
}

public function store(Request $request) {
    $newPost = $request->all();

    $newPost['user_id'] = Auth::id();
    $newPost['category_id'] = $request->get('category_id');
    $newPost['title'] = $request->get('title');
    $newPost['body'] = $request->get('body');

    Post::create($newPost);

    return redirect(route('posts'));
}

public function delete($id) {
    $post = Post::find($id);

    $post->delete();

    return redirect(route('posts'));
}
}
