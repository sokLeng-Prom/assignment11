<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index() {
        $categories = Category::all();

        return view('categories', [
            'categories' => $categories
        ]);
    }

    public function create() {
        return view('categories-create');
    }

    public function editPage($id) {
        $category = Category::find($id);

        return view('categories-edit', [
            'category' => $category
        ]);
    }

    public function store(Request $request) {
        $newCategory = $request->all();

        Category::create($newCategory);

        return redirect(route('categories'));
    }

    public function edit(Request $request, $id) {
        $category = Category::find($id);

        $category->name = $request->get('name');
        $category->save();

        return redirect(route('categories'));
    }

    public function delete($id) {
        $category = Category::find($id);

        $category->delete();

        return redirect(route('categories'));
    }
}
